(function()
{
	var loginModule = angular.module("login", ["LocalStorageModule"]);
	loginModule.controller("loginCtrl", ["$scope", "$window", "loginService","localStorageService", function($scope, $window, loginService, localStorageService)
	{
		$scope.user = {};
		$scope.submitLogin = function()
		{
			loginService.login($scope.user.uname, $scope.user.password).then(function(response)
				{
					console.log("Login success");
					localStorageService.set("loginToken", response.data);
					$window.location.href = '/layout2/standard/contacts.html';
				}, function(response)
				{
					console.log("Login failed");
					
				});
		};
	}]);

	// Services
	loginModule.service("loginService", ["$q", "loginModel", function($q, loginModel)
	{
		var lmthis =  this;

		var api = new loginModel();

		lmthis.login = function (username, password)
		    {
		    	var defer = $q.defer();

		        api.login(username, password,  function (response)
		        {
		            return defer.resolve(response);
		        }, function (error)
		        {
		            return defer.reject(error);
		        });

		        return defer.promise;
		    }

	}]);

	// Model
	loginModule.service("loginModel", ["$http", "$window", function($http, $window)
	{
		function loginModel()
		{
		}

		loginModel.prototype = {
			login: function(username, password, callback)
			{
				$http({
						method: 'POST',
						url: 'https://testapi.nzfsg.co.nz/Login?username=' + username + '&password=' + password ,
						headers: {
						}
					}).then(function successCallback(response)
					{
					    return callback(response);
					}, function errorCallback(response)
					{
						$window.location.href = '/layout2/standard/index.html';
					});
			},
		};
		return loginModel;
	}]);
})();	

// $http.post('/api/authenticate', { username: username, password: password })
//                 .success(function (response) {
//                     callback(response);
//                 });
