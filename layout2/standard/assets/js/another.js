
	/*for add client*/
	var popUpAddclient = $(".pop-up-addclient");
	var background = $(".background");
	var saveAndClose = $(".button-save-and-close");

	background.hide();
	popUpAddclient.hide();

	var showAddClientPanel = function()
	{
		popUpAddclient.fadeIn(200);
		background.fadeIn(200);
	}

	var saveAndClose = function()
	{
		background.fadeOut(200);
		popUpAddclient.fadeOut(200);
	}
	/*end of add client*/


	/*for add relationship panel*/
	var popUpAddrel = $(".pop-up-addrelationship");
	var close = $(".button-x-mark");
	var saveAndCloseRel = $(".rel-button-save-and-close");

	popUpAddrel.hide();
	var showAddRelationship = function()
	{
		popUpAddrel.fadeIn(200);
		background.fadeIn(200);
	}

	var closeRel = function()
	{
		popUpAddrel.fadeOut(200);
		background.fadeOut(200);
	}
	/*end of add relationship panel*/

	/*redirects*/
	var trIndex = $(".table-tbody-contact-list tr");
	trIndex.click(function ()
	{
        window.location.href = $(this).data('url');
    });

    var liA = $(".nav-client-info li a");
    liA.click(function()
    {
    	window.location.href = $(this).data('url');
    });
	/**/

	(function()
	{
		var app = angular.module("app", ["login", "LocalStorageModule"]);
		
		// Model
		app.service("contactModel", ['$http', 'localStorageService', function($http, localStorageService)
		{
			var token = localStorageService.get("loginToken");
			
			// Start prototype
		    function contactModel() {
		    }

		    contactModel.prototype = {
    			FamilyListGet : function(startWith, callback)
    			{
					$http({
						method: 'GET',
						url: 'https://testapi.nzfsg.co.nz/contacts/FamilyListGet?startWith=' + startWith,
						headers: {
							'Authorization': 'Bearer ' + token
						}
					}).then(function successCallback(response)
					{
					    return callback(response);
					}, function errorCallback(response)
					{
					});
				},
			}

			return contactModel;
		}]);

		// Service
		app.service("contactService", ['$q', 'contactModel', function($q, contactModel)
		{

			var factory = this;

		    var api = new contactModel();

		    factory.FamilyListGet = function (startWith)
		    {
		    	var defer = $q.defer();

		        api.FamilyListGet(startWith, function (response)
		        {
		            factory.data = response.data;
		            return defer.resolve(response);
		        }, function (error)
		        {
		            factory.data = [];
		            return defer.reject(error);
		        });

		        return defer.promise;
		    }
		}]);
		app.service("redirectToSummaryService", ["$window", function($window)
		{
			var rts = this;

			this.redirectToSummary = function(familyID)
			{
				$window.location.href = '/layout2/standard/contact-person-summary.html?famId=' + familyID;
			}
		}]);
		
			
		// Controllers
		app.controller("index", Index);
		Index.$inject = ['$scope', '$filter', 'contactService', 'redirectToSummaryService'];
		function Index($scope, $filter, contactService, redirectToSummaryService)
		{
			// $scope.defaultFilter = 'a';
			$scope.getFamily = function(letter) {
				contactService.FamilyListGet(letter).then(function (response)
				{
					$scope.contacts = response.data.FamilyList;
				});	
			};

			// init
			$scope.getFamily('a');
			

			$scope.redirectToSummary = function(familyID)
			{
				redirectToSummaryService.redirectToSummary(familyID);
			}
			$scope.filterLetter = function(letter)
			{
				// $scope.defaultFilter = letter;
				$scope.getFamily(letter);
				$scope.selected = letter;
			}
		}
	})();

